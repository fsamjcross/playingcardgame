'use strict';
var suits = ["hearts", "diamonds", "clubs", "spades"];
var ranks = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
var names = ['Ace', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'jack', 'queen', 'king'];


/**
 * makes a deck of 52 cards 
 * @returns {deck} The deck the has been genarated 
 */
function makeDeck() {
    var i = 0, j = 0;
    var deck = [];
    //for each type of suit
    for (i = 0; i < suits.length; i++) {
        //and for each rank
        for (j = 0; j < ranks.length; j++) {
            //make a card
            var card = {};
            card.suit = suits[i];
            card.rank = ranks[j];
            card.names = names[j];
            card.url = 'cards/' + names[j] + '_of_' + suits[i] + '.svg';
            deck.push(card);
        }
    }
    return deck;
};
/**
 * makes a card
 * @param   {string} suit the suit of the card being made
 * @param   {number} rank the rank of the card in number
 * @param   {string} name The name of the card e.g jack,queen,king,1
 * @returns {card}   the card that has been made
 */
function makeCard(suit, rank, name) {
    return {
        suit: suit,
        rank: rank,
        name: name
    };
};
/**
 * gets a random number between two numbers
 * @param   {number} min the starting number 
 * @param   {number} max the ending number 
 * @returns {number} the random number
 */
function randomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
};
/**
 * picks a random card out of a deck of cards and removes it from the array
 * @param   {Array}    deck The deck of cards to pick from
 * @returns {card} the random card
 */
function pickcard(deck) {
    var number = randomIntFromInterval(0, deck.length - 1);
    var card = deck[number];
    deck.splice(number, 1);
    return card;
};
/**
 * checks to see of the cards are all of the same kinds
 * @param   {card}  card1 
 * @param   {card}  card2 
 * @param   {card}  card3 
 * @returns {boolean}
 */
function ifThreeOfaKind(card1, card2, card3) {
    if (card1.rank == card2.rank && card1.rank == card3.rank) {
        return true;
    };
    return false;
};
/**
 * checks if the cards are sequential 
 * @param   {card} card1 
 * @param   {card} card2 
 * @param   {card} card3 
 * @returns {boolean}  
 */
function ifStraight(card1, card2, card3) {
    var test = sortcards([card1, card2, card3])
    if (test[0].rank + 1 == test[1].rank && (test[1].rank + 1 == test[2].rank || test[1].rank + 1 == 1)) {
        if (test[0].suit == test[1].suit && test[1].suit == test[2].suit) {
            return false;
        } else {
            return true;
        }

    }
    return false;
};
/**
 * checks if the cards are sequential and the same suit
 * @param   {card} card1 
 * @param   {card} card2 
 * @param   {card} card3 
 * @returns {boolean}  
 */
function ifStraightFlush(card1, card2, card3) {
    var test = sortcards([card1, card2, card3])
    if (test[0].rank + 1 == test[1].rank && test[1].rank + 1 == test[2].rank) {
        if (test[0].suit == test[1].suit && test[1].suit == test[2].suit) {
            return true;
        }
    }
    return false;
};
/**
 * checks if the cards are the same suit
 * @param   {card} card1 
 * @param   {card} card2 
 * @param   {card} card3 
 * @returns {boolean}  
 */
function Flush(card1, card2, card3) {
    if (card1.suit == card2.suit && card1.suit == card3.suit) {
        return true;
    };
    return false;
};
/**
 * checks if two of the cards are the same suit
 * @param   {card} card1 
 * @param   {card} card2 
 * @param   {card} card3 
 * @returns {boolean}  
 */
function Pair(card1, card2, card3) {
    if (card1.rank == card2.rank || card1.rank == card3.rank) {
        return true;
    };
    return false;
};
/**
 * checks what kind of win out have if any 
 * @param   {card} card1 
 * @param   {card} card2 
 * @param   {card} card3 
 * @returns {boolean}  
 */
function check(card1, card2, card3) {
    var word
    var score
    if (ifThreeOfaKind(card1, card2, card3)) {
        word = 'Three Of a Kind'
        score = 5
    } else if (ifStraight(card1, card2, card3)) {
        word = 'Straight'
        score = 4
    } else if (ifStraightFlush(card1, card2, card3)) {
        word = 'Straight Flush'
        score = 3
    } else if (Flush(card1, card2, card3)) {
        word = 'Flush'
        score = 3
    } else if (Pair(card1, card2, card3)) {
        word = 'Pair'
        score = 2
    } else {
        word = 'no win'
        score = 1
    }
    return [score, word]
}
/**
 * takes 3 cards from a new deck
 * @returns {Array} array of cards
 */
function dealhand(deck) {
    
    return [pickcard(deck), pickcard(deck), pickcard(deck)];
}
/**
 * sorts the cards in rank order
 * @param   {Array}    cards the array of cards
 * @returns {array} the cards in order of rank
 */
function sortcards(cards) {
    return cards.sort(function (a, b) {
        return a.rank - b.rank;
    });
}

var deck = makeDeck()
var hand = dealhand(deck);
new Vue({
    el: '#app',
    data: {
        hand: dealhand(deck),
    },
    computed: {
        check:function(){
           return check(this.hand[0], this.hand[1], this.hand[2]);
        }
    },

    methods: {
        /**
         * deals cards to the user
         * @param {event} event the event from vue
         */
        deal: function (event) {
            this.hand = sortcards(dealhand(deck))
        }
    }
})